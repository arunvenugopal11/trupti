const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        'hello-wold': './src/hello-world.js',
        'home': './src/home.js',
        'coming-soon': './src/coming-soon.js',
        'connect': './src/connect.js',
        'hear': './src/hear.js',
        'sieva': './src/sieva.js',
        'personal-branding': './src/personal-branding.js',
        'work': './src/work.js',
        'connect-with-coffee': './src/connect-with-coffee.js',
        'schedule-session': './src/schedule-session.js',
        'zappie': './src/zappie.js',
        'moodilics': './src/moodilics.js',
        'consultation': './src/consultation.js',
        'freebies': './src/freebies.js',
        'azap': './src/azap.js',
        'design-services': './src/design-services.js',
        'contact': './src/contact.js',
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, './dist'),
        publicPath: ''
    },
    mode: 'development',
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),
        index: 'index.html',
        port: 9000
    },
    module: {
        rules: [{
                test: /\.(png|jpg|svg)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader', 'css-loader'
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader', 'css-loader', 'sass-loader'
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/env'],
                        plugins: ['transform-class-properties']
                    }
                }
            },
            {
                test: /\.hbs$/,
                use: [
                    'handlebars-loader'
                ]
            },
            {
                test: /\.woff(2)?(\?[a-z0-9]+)?$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff"
            }, {
                test: /\.(ttf|eot|svg)?(\?[a-z0-9#=&.]+)?$/,
                loader: "file-loader"
            }
        ]
    },
    // context: path.join(__dirname, 'src'),
    plugins: [
        new CleanWebpackPlugin('dist'),
        new HtmlWebpackPlugin({
            filename: 'coming-soon.html',
            chunks: ['coming-soon'],
            title: "Trupti Shirodkar's Portfolio | Work in Progress ",
            template: 'src/coming-soon.hbs',
            description: ''
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            chunks: ['home', 'vendors_home'], //chunks names are specified in entrypoint object
            title: "Trupti Shirodkar -  Product Designer | User Experience Designer",
            template: 'src/index.hbs',
            description: 'Currently in Toronto, Canada, A product designer loves developing concepts and working on design projects to impact lives and create changes'
        }),
        new HtmlWebpackPlugin({
            filename: 'connect.html',
            chunks: ['connect'], //chunks names are specified in entrypoint object
            title: "Connect With Us",
            template: 'src/connect.hbs',
            description: 'Connect With Us'
        }),
        new HtmlWebpackPlugin({
            filename: 'hear.html',
            chunks: ['hear'], //chunks names are specified in entrypoint object
            title: "Hear From You",
            template: 'src/hear.hbs',
            description: 'Hear From You'
        }),
        new HtmlWebpackPlugin({
            filename: 'sieva.html',
            chunks: ['sieva'], //chunks names are specified in entrypoint object
            title: "Sieva | Work Page",
            template: 'src/sieva.hbs',
            description: 'Sieva | Skin care specially made for you'
        }),
        new HtmlWebpackPlugin({
            filename: 'personal-branding.html',
            chunks: ['personal-branding'], //chunks names are specified in entrypoint object
            title: "Personal Branding | Work Page",
            template: 'src/personal-branding.hbs',
            description: 'Personal Branding | Taste of my design'
        }),
        new HtmlWebpackPlugin({
            filename: 'work.html',
            chunks: ['work'], //chunks names are specified in entrypoint object
            title: "Check out my work - Trupsfolio",
            template: 'src/work.hbs',
            description: 'I am Trupti Shirodkar (owner of Trupsfolio), passionate about building products, especially which are driven by the purpose to empower people'
        }),
        new HtmlWebpackPlugin({
            filename: 'connect-with-coffee.html',
            chunks: ['connect-with-coffee'], //chunks names are specified in entrypoint object
            title: "Connect With Coffee",
            template: 'src/connect-with-coffee.hbs',
            description: 'Connect With Coffee | Trupti Shirodkar'
        }),
        new HtmlWebpackPlugin({
            filename: 'schedule-session.html',
            chunks: ['schedule-session'], //chunks names are specified in entrypoint object
            title: "Schedule a Session",
            template: 'src/schedule-session.hbs',
            description: 'Schedule a session with Trupti Shirodkar | UI/UX, Brand Designer'
        }),
        new HtmlWebpackPlugin({
            filename: 'zappie.html',
            chunks: ['zappie'], //chunks names are specified in entrypoint object
            title: "Zappie | Work Page",
            template: 'src/zappie.hbs',
            description: 'Zappie | Connecting children with bed time stories'
        }),
        new HtmlWebpackPlugin({
            filename: 'moodilics.html',
            chunks: ['moodilics'], //chunks names are specified in entrypoint object
            title: "Moodilics | Work Page",
            template: 'src/moodilics.hbs',
            description: 'Moodilics | Changing the experience to buy ice cream'
        }),
        new HtmlWebpackPlugin({
            filename: 'consultation.html',
            chunks: ['consultation'], //chunks names are specified in entrypoint object
            title: "Helping creative build UI/UX skills - Trupti Shirodkar (Trupsfolio)",
            template: 'src/consultation.hbs',
            description: 'Get on a call with me and check out my journey on Instagram (Trupsfolio) where I share my insights to upscale UI/UX design skills'
        }),
        new HtmlWebpackPlugin({
            filename: 'freebies.html',
            chunks: ['freebies'], //chunks names are specified in entrypoint object
            title: "Freebies",
            template: 'src/freebies.hbs',
            description: 'Freebies'
        }),
        new HtmlWebpackPlugin({
            filename: 'azap.html',
            chunks: ['azap'], //chunks names are specified in entrypoint object
            title: "Azap",
            template: 'src/azap.hbs',
            description: 'Azap'
        }),
        new HtmlWebpackPlugin({
            filename: 'design-services.html',
            chunks: ['design-services'], //chunks names are specified in entrypoint object
            title: "Design Services offered by Trupti Shirodkar (Trupsfolio)",
            template: 'src/design-services.hbs',
            description: 'Phases of product design which include discovery, ideation, design , prototyping and testing. User experience and user interface designer in love with the process.'
        }),
        new HtmlWebpackPlugin({
            filename: 'contact.html',
            chunks: ['contact'], //chunks names are specified in entrypoint object
            title: "Get in touch with me - Trupti Shirodkar (Trupsfolio)",
            template: 'src/contact.hbs',
            description: 'Need design services? If you need UX strategy, UI design and prototyping services for your idea to be polished and launched into the market.'
        }),
        new CopyWebpackPlugin([{
                from: "src/images",
                to: "images"
            },
            {
                context: "./src",
                from: "**/*.php",
                to: "./"
            },
            {
                context: "./src",
                from: "composer.json",
                to: "./"
            }
        ], {
            copyUnmodified: true
        }),
        new CopyWebpackPlugin([{
            from: 'src/videos',
            to: 'videos'
        }]),
        new CopyWebpackPlugin([{
            from: 'src/pdf',
            to: 'pdf'
        }])
    ]
}