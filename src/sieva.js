import "bootstrap/dist/css/bootstrap.min.css";
import "./scss/ti-styles.scss";
import $ from "jquery";
import "bootstrap";
import 'pace-js';
Pace.start({
    document: false,
    target: '.ti-preloader__progress-count',
    minTime: 9000,
});
Pace.on("done", function () {
    $(".ti-preloader__content").hide();
    $(".ti-preloader-section").fadeOut(1200);
});
import {
    TweenMax,
    TimelineLite,
    TimelineMax,
    CSSPlugin,
    ScrollToPlugin,
    Draggable,
    Elastic
} from "gsap/all";
import EasePack from "gsap/EasePack";
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick.min.js';

const plugins = [CSSPlugin, ScrollToPlugin, EasePack];



(function ($) {

    $(document).ready(function () {
        // Some help functions.
        const shuffleArray = arr => arr.sort(() => Math.random() - 0.5);
        const lineEq = (y2, y1, x2, x1, currentVal) => {
            let m = (y2 - y1) / (x2 - x1);
            let b = y1 - m * x1;
            return m * currentVal + b;
        };
        const lerp = (a, b, n) => (1 - n) * a + n * b;
        const body = document.body;
        const bodyColor = getComputedStyle(body).getPropertyValue('--color-bg').trim() || 'white';
        const getMousePos = (e) => {
            let posx = 0;
            let posy = 0;
            if (!e) e = window.event;
            if (e.pageX || e.pageY) {
                posx = e.pageX;
                posy = e.pageY;
            } else if (e.clientX || e.clientY) {
                posx = e.clientX + body.scrollLeft + document.documentElement.scrollLeft;
                posy = e.clientY + body.scrollTop + document.documentElement.scrollTop;
            }
            return {
                x: posx,
                y: posy
            }
        }

        
        //Get the button:
        var mybutton = document.getElementById("scrollTop");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function () { scrollFunction() };

        function scrollFunction() {
            if (document.body.scrollTop > 700 || document.documentElement.scrollTop > 700) {
                mybutton.style.display = "block";
            } else {
                mybutton.style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        $("#scrollTop").click(function () {
            //     document.body.scrollTop = 0; // For Safari
            //   document.documentElement.scrollTop = 0;
            window.scrollTo({ top: 0, behavior: 'smooth' });
        });
        class CursorFx {
            constructor(el) {
                this.DOM = {
                    el: el
                };
                this.DOM.dot = this.DOM.el.querySelector('.cursor__inner--dot');
                this.DOM.circle = this.DOM.el.querySelector('.cursor__inner--circle');
                this.bounds = {
                    dot: this.DOM.dot.getBoundingClientRect(),
                    circle: this.DOM.circle.getBoundingClientRect()
                };
                this.scale = 1;
                // this.opacity = 1;
                this.mousePos = {
                    x: 0,
                    y: 0
                };
                this.lastMousePos = {
                    dot: {
                        x: 0,
                        y: 0
                    },
                    circle: {
                        x: 0,
                        y: 0
                    }
                };
                this.lastScale = 1;
                this.lastOpacity = 1;

                this.initEvents();
                requestAnimationFrame(() => this.render());
            }
            initEvents() {
                window.addEventListener('mousemove', ev => this.mousePos = getMousePos(ev));
            }
            render() {
                this.lastMousePos.dot.x = lerp(this.lastMousePos.dot.x, this.mousePos.x - this.bounds.dot.width / 2, 1);
                this.lastMousePos.dot.y = lerp(this.lastMousePos.dot.y, this.mousePos.y - this.bounds.dot.height / 2, 1);
                this.lastMousePos.circle.x = lerp(this.lastMousePos.circle.x, this.mousePos.x - this.bounds.circle.width / 2, 0.15);
                this.lastMousePos.circle.y = lerp(this.lastMousePos.circle.y, this.mousePos.y - this.bounds.circle.height / 2, 0.15);
                this.lastScale = lerp(this.lastScale, this.scale, 0.15);
                this.lastOpacity = lerp(this.lastOpacity, this.opacity, 0.1);
                this.DOM.dot.style.transform = `translateX(${(this.lastMousePos.dot.x)}px) translateY(${this.lastMousePos.dot.y}px)`;
                this.DOM.circle.style.transform = `translateX(${(this.lastMousePos.circle.x)}px) translateY(${this.lastMousePos.circle.y}px) scale(${this.lastScale})`;
                this.DOM.circle.style.opacity = this.lastOpacity
                requestAnimationFrame(() => this.render());
            }
            enter() {
                cursor.scale = 2.7;
                // cursor.querySelector('.cursor__inner--dot').scale = 2.7;
            }
            leave() {
                cursor.scale = 1;
            }
            click() {
                this.lastScale = 1;
                this.lastOpacity = 0;
            }
        }
        // const cursor = new CursorFx(document.querySelector('.cursor'));
        // [...document.querySelectorAll('[data-hover]')].forEach((link) => {
        //     link.addEventListener('mouseenter', () => cursor.enter() );
        //     link.addEventListener('mouseleave', () => cursor.leave() );
        //     link.addEventListener('click', () => cursor.click() );
        // });
    });

    //   Section Four Slider
    $('.ti-work-section-six__cards-list').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        autoplay: false,
        autoplaySpeed: 8000,
        pauseOnHover: false,
        PauseOnFocus: false,
        focusOnSelect: false,
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1008,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ],
        prevArrow: '<button data-hover type="button" class="ti-slick-arrow ti-slick-arrow--prev"></button>',
        nextArrow: '<button data-hover type="button" class="ti-slick-arrow ti-slick-arrow--next"><svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" viewBox="-1 -1 76 76"><circle class="ti-slick-arrow__circle" cx="34" cy="34" r="34" fill="none" fill-rule="evenodd" stroke="#b7fa82" stroke-width="2px" opacity="1" transform="translate(1 1)" style="stroke-dashoffset: 252px;"></circle></svg></button>',
    });

    var autoplayTl = new TimelineMax(); // Slider Arrow Ring Animation
    autoplayTl.fromTo('.ti-slick-arrow__circle', 8.6, {
        "stroke-dashoffset": 220,
        ease: Power0.easeNone
    }, {
        "stroke-dashoffset": 440,
        ease: Power0.easeNone
    });
    // autoplayTl.repeat(-1);
    autoplayTl.play();
    autoplayTl.addCallback(goToNextSlide);

    function goToNextSlide() {
        $('.ti-work-section-six__cards-list').slick('slickNext');
    }

    $('.ti-work-section-six__cards-list').on('afterChange', function (event, slick, currentSlide, nextSlide) {
        autoplayTl.seek(0);
        autoplayTl.play();
        $('body').focus();
    });

    //   Section Ten Slider (on mobile resolutions)
    if (window.matchMedia("(max-width: 767px)").matches) {
        $('.ti-work-section-ten__grid-wrapper').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true,
            autoplay: true,
            autoplaySpeed: 8000,
            pauseOnHover: false,
            PauseOnFocus: false,
            focusOnSelect: false,
            responsive: [{
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 1008,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ],
            prevArrow: '<button data-hover type="button" class="ti-slick-arrow ti-slick-arrow--prev"></button>',
            nextArrow: '<button data-hover type="button" class="ti-slick-arrow ti-slick-arrow--next"><svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" viewBox="-1 -1 76 76"><circle class="ti-slick-arrow__circle" cx="34" cy="34" r="34" fill="none" fill-rule="evenodd" stroke="#b7fa82" stroke-width="2px" opacity="1" transform="translate(1 1)" style="stroke-dashoffset: 252px;"></circle></svg></button>',
        });

        $('.ti-work-section-ten__grid-wrapper').on('afterChange', function (event, slick, currentSlide, nextSlide) {
            sliderArrowCircleTl.seek(0);
            sliderArrowCircleTl.play();
            $('body').focus();
        });

        var sliderArrowCircleTl = new TimelineMax(); // Slider Arrow Ring Animation
        sliderArrowCircleTl.fromTo('.ti-work-section--ten .ti-slick-arrow__circle', 8.6, {
            "stroke-dashoffset": 220,
            ease: Power0.easeNone
        }, {
            "stroke-dashoffset": 440,
            ease: Power0.easeNone
        });
        sliderArrowCircleTl.play();
        sliderArrowCircleTl.addCallback(sectionTengoToNextSlide);

        function sectionTengoToNextSlide() {
            $('.ti-work-section-ten__grid-wrapper').slick('slickNext');
        }
    }

    function fixAspectRatio() {
        //16:9
        var videos = $(".ti-work-section-twelve__video-wrapper video");
        var width = videos.width();
        videos.height(width);
    }
    fixAspectRatio();

    $(window).resize(function () {
        fixAspectRatio();
    });
    $(".ti-back-button").click(function () {
        window.history.back();
    });

    $('.ti-work-section-eight__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 8000,
        pauseOnHover: false,
        PauseOnFocus: false,
        focusOnSelect: false,
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1008,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ],
        prevArrow: '<button data-hover type="button" class="ti-slick-arrow ti-slick-arrow--prev"></button>',
        nextArrow: '<button data-hover type="button" class="ti-slick-arrow ti-slick-arrow--next"><svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" viewBox="-1 -1 76 76"><circle class="ti-slick-arrow__circle" cx="34" cy="34" r="34" fill="none" fill-rule="evenodd" stroke="#b7fa82" stroke-width="2px" opacity="1" transform="translate(1 1)" style="stroke-dashoffset: 252px;"></circle></svg></button>',
    });

    $('.ti-work-section-eight__slider').on('afterChange', function (event, slick, currentSlide, nextSlide) {
        sectionEightSliderArrowCircleTl.seek(0);
        sectionEightSliderArrowCircleTl.play();
        $('body').focus();
    });

    var sectionEightSliderArrowCircleTl = new TimelineMax(); // Slider Arrow Ring Animation
    sectionEightSliderArrowCircleTl.fromTo('.ti-work-section--eight .ti-slick-arrow__circle', 8.6, {
        "stroke-dashoffset": 220,
        ease: Power0.easeNone
    }, {
        "stroke-dashoffset": 440,
        ease: Power0.easeNone
    });
    sectionEightSliderArrowCircleTl.play();
    sectionEightSliderArrowCircleTl.addCallback(sectionEightGoToNextSlide);

    function sectionEightGoToNextSlide() {
        $('.ti-work-section-eight__slider').slick('slickNext');
    }

})($);