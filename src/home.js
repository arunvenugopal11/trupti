import $ from "jquery";
import "bootstrap";
import {
  TweenMax,
  TimelineLite,
  TimelineMax,
  CSSPlugin,
  ScrollToPlugin,
  Draggable,
  Elastic
} from "gsap/all";
import "bootstrap/dist/css/bootstrap.min.css";
import "./scss/ti-styles.scss";
import EasePack from "gsap/EasePack";
import 'pace-js';

const plugins = [CSSPlugin, ScrollToPlugin, EasePack];

var paceOptions = {
  // Disable the 'elements' source
  elements: false,

  // Only show the progress on regular and ajax-y page navigation,
  // not every request
  restartOnRequestAfter: false
}
Pace.start({
  document: false,
  target: '.ti-preloader__progress-count',
  minTime: 9000,
});
Pace.on("done", function () {
  $(".ti-preloader__content").hide();
  $(".ti-preloader-section").fadeOut(1200);
});

(function ($) {
  // Set variables
  console.log("check");
  var $activeTextSlide = $(".ti-hero-section__slider-text-right.slider-active"),
    $allTextSlides = $(".ti-hero-section__slider-text-right"),
    $activeTextLeftSlide = $(
      ".ti-hero-section__slider-text-left.slider-active"
    ),
    $allTextLeftSlides = $(".ti-hero-section__slider-text-left"),
    $activeImageSlide = $(".ti-hero-section__slider-image.slider-active"),
    $allImageSlides = $(".ti-hero-section__slider-image"),
    $activeNextSlideThumbnail = $(
      ".ti-hero-section__carousel-next-slide-thumbnail-image.slider-active"
    ),
    $allNextSlideThumbnails = $(
      ".ti-hero-section__carousel-next-slide-thumbnail-image"
    ),
    $hero = $(".ti-hero-section__content-wrapper"),
    totalSlides = $allImageSlides.length,
    currentSlide,
    totalThumbnails = $allNextSlideThumbnails.length,
    currentThumbnail,
    $activeDescriptionSlide = $(".ti-hero-section__description.slider-active"),
    $allDescriptionSlides = $(".ti-hero-section__description"),
    $activeCta = $(".ti-hero-section__cta.slider-active"),
    $allCtas = $(".ti-hero-section__cta");

  // Mouse move tilt effect
  $(document).mousemove(function (event) {
    // Detect mouse position
    var xPos = event.clientX / $(window).width() - 0.5;
    var yPos = event.clientY / $(window).height() - 0.5;

    // Tilt elements on hero container
    TweenLite.to($hero, 0.6, {
      rotationY: 3 * xPos,
      rotationX: 3 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 900,
      transformOrigin: "center"
    });
    TweenLite.to(".ti-hero-section__slider-text-right", 0.6, {
      rotationY: 30 * xPos,
      rotationX: 30 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 200,
      transformOrigin: "left"
    });
    TweenLite.to(".ti-hero-section__slider-text-left", 0.6, {
      rotationY: 30 * xPos,
      rotationX: 30 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 200,
      transformOrigin: "right"
    });
    TweenLite.to(".ti-hero-section__slider-images", 0.6, {
      rotationY: 3 * xPos,
      rotationX: 3 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 200,
      transformOrigin: "top"
    });
    TweenLite.to(".ti-hero-section__carousel-next-slide-thumbnail-image", 0.6, {
      rotationY: 20 * xPos,
      rotationX: 20 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 200,
      transformOrigin: "center"
    });
    TweenLite.to(".ti-hero-section__particles-wrapper", 0.6, {
      rotationY: 2 * xPos,
      rotationX: 2 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 900,
      transformOrigin: "center"
    });
    TweenLite.to(".ti-hero-section__carousel-nav-count", 0.6, {
      rotationY: 20 * xPos,
      rotationX: 20 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 200,
      transformOrigin: "center"
    });
    TweenLite.to(".ti-hero-section__details", 0.6, {
      rotationY: 10 * xPos,
      rotationX: 10 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 900,
      transformOrigin: "center"
    });
  });

  // Slider Autoplay
  var autoplayTl = new TimelineMax();
  autoplayTl.fromTo('.ti-hero-section__carousel-next-slide-thumbnail-circle', 9, {
    "stroke-dashoffset": 220,
    ease: Power0.easeNone
  }, {
    "stroke-dashoffset": 440,
    ease: Power0.easeNone
  });
  autoplayTl.repeat(-1)
  // autoplayTl.to('.ti-hero-section__carousel-next-slide-thumbnail-circle', 5,  {"stroke-dashoffset": 440, ease: Power0.easeNone});
  autoplayTl.play();
  autoplayTl.addCallback(goToNextSlide);

  // Init function that run on page load
  function init() {
    // Set the total slide count
    $(".ti-hero-section__carousel-nav-count__total-slides").html(
      "0" + totalSlides
    );
    // Current Slide & Thumbnail
    currentSlide = $(".ti-hero-section__slider-images").attr("data-slide");
    currentThumbnail = $(
      ".ti-hero-section__carousel-next-slide-thumbnail"
    ).attr("data-slide");

    // Hide all slides apart from the active one
    TweenLite.set($allImageSlides.not($activeImageSlide), {
      autoAlpha: 0
    });
    TweenLite.set($allTextSlides.not($activeTextSlide), {
      autoAlpha: 0
    });
    TweenLite.set($allTextLeftSlides.not($activeTextLeftSlide), {
      autoAlpha: 0
    });
    TweenLite.set($allNextSlideThumbnails.not($activeNextSlideThumbnail), {
      autoAlpha: 0
    });
    TweenLite.set($allDescriptionSlides.not($activeDescriptionSlide), {
      autoAlpha: 0
    });
    TweenLite.set($allCtas.not($activeCta), {
      autoAlpha: 0
    });
  }

  // Run Init function
  init();

  // Go to next slide - pass 2 parameters - slideOut and slideIn
  function goToNextSlide() {
    autoplayTl.seek(0);
    // autoplayTl.play();
    // Last Slide to First Slide
    if (totalSlides == currentSlide + 1) {
      console.log("inside if");
      currentSlide = 0;
      TweenMax.to(
        $(".ti-hero-section__slider-image.slider-active"),
        0.8, {
          autoAlpha: 0,
          ease: Sine.easeOut
        },
        0
      );
      TweenMax.fromTo(
        $(".ti-hero-section__slider-text-right.slider-active"),
        0.8, {
          autoAlpha: 1,
          left: "0",
          ease: Sine.easeOut
        }, {
          autoAlpha: 0,
          left: "20px",
          ease: Sine.easeOut
        },
        0
      );
      TweenMax.fromTo(
        $(".ti-hero-section__slider-text-left.slider-active"),
        0.8, {
          autoAlpha: 1,
          right: "0",
          ease: Sine.easeOut
        }, {
          autoAlpha: 0,
          right: "20px",
          ease: Sine.easeOut
        },
        0
      );
      TweenMax.to(
        $(".ti-hero-section__description.slider-active"),
        0.8, {
          autoAlpha: 0,
          ease: Sine.easeOut
        },
        0
      );
      TweenMax.to(
        $(".ti-hero-section__cta.slider-active"),
        0.8, {
          autoAlpha: 0,
          ease: Power0.easeNone
        },
        0
      );
      $(".ti-hero-section__slider-image.slider-active").removeClass(
        "slider-active"
      );
      $(".ti-hero-section__slider-text-right.slider-active").removeClass(
        "slider-active"
      );
      $(".ti-hero-section__slider-text-left.slider-active").removeClass(
        "slider-active"
      );
      $(".ti-hero-section__description.slider-active").removeClass(
        "slider-active"
      );
      $(".ti-hero-section__cta.slider-active").removeClass("slider-active");
      TweenMax.to(
        ".ti-hero-section__slider-images img:first-child",
        0.8, {
          autoAlpha: 1,
          ease: Sine.easeOut
        },
        "-=0.5"
      );
      TweenMax.fromTo(
        ".ti-hero-section__slider-texts-right h4:first-child",
        0.8, {
          autoAlpha: 0,
          left: "-20px",
          ease: Sine.easeOut
        }, {
          autoAlpha: 1,
          left: "0px",
          ease: Sine.easeOut
        },
        "-=0.5"
      );
      TweenMax.fromTo(
        ".ti-hero-section__slider-texts-left h4:first-child",
        0.8, {
          autoAlpha: 0,
          right: "-20px",
          ease: Sine.easeOut
        }, {
          autoAlpha: 1,
          right: "0px",
          ease: Sine.easeOut
        },
        "-=0.5"
      );
      TweenMax.fromTo(
        ".ti-hero-section__descriptions p:first-child",
        0.8, {
          autoAlpha: 0,
          top: "20px",
          ease: Sine.easeOut
        }, {
          autoAlpha: 1,
          top: "0",
          ease: Sine.easeOut
        },
        "-=0.5"
      );
      TweenMax.fromTo(
        ".ti-hero-section__cta-wrapper a:first-child",
        0.8, {
          autoAlpha: 0,
          top: "20px",
          ease: Sine.easeOut
        }, {
          autoAlpha: 1,
          top: "0",
          ease: Sine.easeOut
        },
        "-=0.5"
      );
      $(
        ".ti-hero-section__slider-images .ti-hero-section__slider-image:first"
      ).addClass("slider-active");
      $(
        ".ti-hero-section__slider-texts-right .ti-hero-section__slider-text-right:first"
      ).addClass("slider-active");
      $(
        ".ti-hero-section__slider-texts-left .ti-hero-section__slider-text-left:first"
      ).addClass("slider-active");
      $(".ti-hero-section__descriptions p:first-child").addClass(
        "slider-active"
      );
      $(".ti-hero-section__cta-wrapper a:first-child").addClass(
        "slider-active"
      );
      $(".ti-hero-section__carousel-nav-count__current-slide").html("01");
    } else {
      currentSlide++;
      var newCurrentSlide = currentSlide + 1;
      $(".ti-hero-section__carousel-nav-count__current-slide").html(
        "0" + newCurrentSlide
      );
      var slideOutImage = $(".ti-hero-section__slider-image.slider-active"),
        slideInImage = $(".ti-hero-section__slider-image.slider-active").next(
          ".ti-hero-section__slider-image"
        ),
        slideOutRightText = $(
          ".ti-hero-section__slider-text-right.slider-active"
        ),
        slideInRightText = $(
          ".ti-hero-section__slider-text-right.slider-active"
        ).next(".ti-hero-section__slider-text-right"),
        slideOutLeftText = $(
          ".ti-hero-section__slider-text-left.slider-active"
        ),
        slideInLeftText = $(
          ".ti-hero-section__slider-text-left.slider-active"
        ).next(".ti-hero-section__slider-text-left"),
        slideOutDescription = $(".ti-hero-section__description.slider-active"),
        slideInDescription = $(
          ".ti-hero-section__description.slider-active"
        ).next(".ti-hero-section__description"),
        slideOutCta = $(".ti-hero-section__cta.slider-active"),
        slideInCta = $(".ti-hero-section__cta.slider-active").next(
          ".ti-hero-section__cta"
        );

      var tl = new TimelineLite();
      // go to the next slide timeline
      tl
        // add class active to the upcoming slide (slideIn)
        .set(slideInImage, {
          className: "+=slider-active"
        })
        // remove class active from the currently active slide (slideOut)
        .set(slideOutImage, {
          className: "-=slider-active"
        })
        // Fade out active slide
        .to(slideOutImage, 0.8, {
          autoAlpha: 0,
          ease: Sine.easeOut
        }, 0)
        // Fade in upcoming slide
        .to(slideInImage, 0.8, {
          autoAlpha: 1,
          ease: Sine.easeOut
        }, "-=0.5")
        // add class active to the upcoming slide (slideIn)
        .set(slideInRightText, {
          className: "+=slider-active"
        })
        // remove class active from the currently active slide (slideOut)
        .set(slideOutRightText, {
          className: "-=slider-active"
        })
        // Fade out active slide
        .fromTo(
          slideOutRightText,
          0.8, {
            autoAlpha: 1,
            left: "0",
            ease: Sine.easeOut
          }, {
            autoAlpha: 0,
            left: "20px",
            ease: Sine.easeOut
          },
          0
        )
        // Fade in upcoming slide
        .fromTo(
          slideInRightText,
          0.8, {
            autoAlpha: 0,
            left: "-20px",
            ease: Sine.easeOut
          }, {
            autoAlpha: 1,
            left: "0px",
            ease: Sine.easeOut
          },
          "-=0.5"
        )
        // add class active to the upcoming slide (slideIn)
        .set(slideInLeftText, {
          className: "+=slider-active"
        })
        // remove class active from the currently active slide (slideOut)
        .set(slideOutLeftText, {
          className: "-=slider-active"
        })
        // Fade out active slide
        .fromTo(
          slideOutLeftText,
          0.8, {
            autoAlpha: 1,
            right: "0",
            ease: Sine.easeOut
          }, {
            autoAlpha: 0,
            right: "20px",
            ease: Sine.easeOut
          },
          0
        )
        // Fade in upcoming slide
        .fromTo(
          slideInLeftText,
          0.8, {
            autoAlpha: 0,
            right: "-20px",
            ease: Sine.easeOut
          }, {
            autoAlpha: 1,
            right: "0px",
            ease: Sine.easeOut
          },
          "-=0.8"
        )
        // add class active to the upcoming slide (slideIn)
        .set(slideInDescription, {
          className: "+=slider-active"
        })
        // remove class active from the currently active slide (slideOut)
        .set(slideOutDescription, {
          className: "-=slider-active"
        })
        // Fade out active slide
        .to(slideOutDescription, 0.8, {
          autoAlpha: 0,
          ease: Sine.easeOut
        }, 0)
        // Fade in upcoming slide
        .fromTo(
          slideInDescription,
          0.8, {
            autoAlpha: 0,
            top: "20px",
            ease: Sine.easeOut
          }, {
            autoAlpha: 1,
            top: "0",
            ease: Sine.easeOut
          }
        )
        // add class active to the upcoming slide (slideIn)
        .set(slideInCta, {
          className: "+=slider-active"
        })
        // remove class active from the currently active slide (slideOut)
        .set(slideOutCta, {
          className: "-=slider-active"
        })
        // Fade out active slide
        .to(slideOutCta, 0.2, {
          autoAlpha: 0,
          ease: Power0.easeNone
        }, 0)
        // Fade in upcoming slide
        .fromTo(
          slideInCta,
          0.8, {
            autoAlpha: 0,
            top: "20px",
            ease: Power0.easeNone
          }, {
            autoAlpha: 1,
            top: "0",
            ease: Power0.easeNone
          },
          "-=0.5"
        );
    }
    // Next Thumbnail Transition
    function goToNextThumbnail() {
      // Last Thumbnail to First Thumbnail
      if (totalThumbnails == currentThumbnail + 2) {
        currentThumbnail = -1;
        TweenMax.to(
          $(
            ".ti-hero-section__carousel-next-slide-thumbnail-image.slider-active"
          ),
          0.8, {
            autoAlpha: 0,
            ease: Sine.easeOut
          },
          0
        );
        $(
          ".ti-hero-section__carousel-next-slide-thumbnail-image.slider-active"
        ).removeClass("slider-active");
        TweenMax.to(
          ".ti-hero-section__carousel-next-slide-thumbnail img:first-child",
          0.8, {
            autoAlpha: 1,
            ease: Sine.easeOut
          },
          "-=0.5"
        );
        $(
          ".ti-hero-section__carousel-next-slide-thumbnail img:first-child"
        ).addClass("slider-active");
      } else {
        currentThumbnail++;
        var slideOutNextThumbnail = $(
            ".ti-hero-section__carousel-next-slide-thumbnail-image.slider-active"
          ),
          slideInNextThumbnail = $(
            ".ti-hero-section__carousel-next-slide-thumbnail-image.slider-active"
          ).next(".ti-hero-section__carousel-next-slide-thumbnail-image");

        var thumbTl = new TimelineLite();
        // go to the next slide timeline
        thumbTl
          // add class active to the upcoming slide (slideIn)
          .set(slideInNextThumbnail, {
            className: "+=slider-active"
          })
          // remove class active from the currently active slide (slideOut)
          .set(slideOutNextThumbnail, {
            className: "-=slider-active"
          })
          // Fade out active slide
          .to(slideOutNextThumbnail, 0.8, {
            autoAlpha: 0,
            ease: Sine.easeOut
          }, 0)
          // Fade in upcoming slide
          .to(
            slideInNextThumbnail,
            0.8, {
              autoAlpha: 1,
              ease: Sine.easeOut
            },
            "-=0.5"
          );
      }
    }
    goToNextThumbnail();

  }
  $(".ti-hero-section__carousel-next-slide-thumbnail").click(function () {
    goToNextSlide();
    // goToNextThumbnail();
  });

  //   Hamburger Dropdown Reveal
  var hamburgerButton = document.querySelector(".ti-header__hamburger");
  var dropdownContainer = document.querySelector(
    ".ti-header-dropdown-container"
  );
  var overlay = document.querySelector(".shape-overlays");
  var paths = document.querySelectorAll(".shape-overlays__path");
  var numPoints = 10;
  var numPaths = paths.length;
  var delayPointsMax = 0.3;
  var delayPerPath = 0.25;
  var duration = 0.9;
  var isOpened = false;
  var pointsDelay = [];
  var allPoints = [];
  var ease = Power2.easeInOut;

  var tl = new TimelineMax({
    onUpdate: render
  });
  var itemsTl = new TimelineMax();

  for (var i = 0; i < numPaths; i++) {
    var points = [];
    allPoints.push(points);
    for (var j = 0; j < numPoints; j++) {
      points.push(100);
    }
  }

  hamburgerButton.addEventListener("click", onClick);

  function onClick() {
    if (!tl.isActive()) {
      isOpened = !isOpened;
      toggle();
    }
  }

  function toggle() {
    $("body").toggleClass("ti-dropdown-active");
    console.log("toggle");
    tl.progress(0).clear();
    itemsTl.progress(0).clear();

    for (var i = 0; i < numPoints; i++) {
      pointsDelay[i] = Math.random() * delayPointsMax;
    }

    for (var i = 0; i < numPaths; i++) {
      var points = allPoints[i];
      var pathDelay = delayPerPath * (isOpened ? i : numPaths - i - 1);

      for (var j = 0; j < numPoints; j++) {
        var config = {
          ease: ease
        };
        config[j] = 0;
        var delay = pointsDelay[j];

        tl.to(points, duration, config, delay + pathDelay);
        if ($("body").hasClass("ti-dropdown-active")) {
          dropdownContainer.style.display = "block";
          tl.eventCallback("onComplete", function () {
            itemsTl.staggerFromTo(
              ".ti-header-dropdown__item-list > li",
              1, {
                y: "90px",
                autoAlpha: 0
              }, {
                y: "0",
                autoAlpha: 1
              },
              "0.3",
              "-=0.2"
            );
            $("body").addClass("ti-dropdown-active");
          });
        } else {
          tl.eventCallback("onComplete", null);
          setTimeout(function () {
            dropdownContainer.style.display = "none";
          }, 1500);
        }
      }
    }
  }

  function render() {
    console.log("render");
    for (var i = 0; i < numPaths; i++) {
      var path = paths[i];
      var points = allPoints[i];

      var d = "";
      d += isOpened ? `M 0 0 V ${points[0]} C` : `M 0 ${points[0]} C`;

      for (var j = 0; j < numPoints - 1; j++) {
        var p = ((j + 1) / (numPoints - 1)) * 100;
        var cp = p - ((1 / (numPoints - 1)) * 100) / 2;
        d += ` ${cp} ${points[j]} ${cp} ${points[j + 1]} ${p} ${points[j + 1]}`;
      }

      d += isOpened ? ` V 100 H 0` : ` V 0 H 0`;
      path.setAttribute("d", d);
    }
  }

  // Some help functions.
  const shuffleArray = arr => arr.sort(() => Math.random() - 0.5);
  const lineEq = (y2, y1, x2, x1, currentVal) => {
    let m = (y2 - y1) / (x2 - x1);
    let b = y1 - m * x1;
    return m * currentVal + b;
  };
  const lerp = (a, b, n) => (1 - n) * a + n * b;
  const body = document.body;
  const bodyColor = getComputedStyle(body).getPropertyValue('--color-bg').trim() || 'white';
  const getMousePos = (e) => {
    let posx = 0;
    let posy = 0;
    if (!e) e = window.event;
    if (e.pageX || e.pageY) {
      posx = e.pageX;
      posy = e.pageY;
    } else if (e.clientX || e.clientY) {
      posx = e.clientX + body.scrollLeft + document.documentElement.scrollLeft;
      posy = e.clientY + body.scrollTop + document.documentElement.scrollTop;
    }
    return {
      x: posx,
      y: posy
    }
  }
  class CursorFx {
    constructor(el) {
      this.DOM = {
        el: el
      };
      this.DOM.dot = this.DOM.el.querySelector('.cursor__inner--dot');
      this.DOM.circle = this.DOM.el.querySelector('.cursor__inner--circle');
      this.bounds = {
        dot: this.DOM.dot.getBoundingClientRect(),
        circle: this.DOM.circle.getBoundingClientRect()
      };
      this.scale = 1;
      // this.opacity = 1;
      this.mousePos = {
        x: 0,
        y: 0
      };
      this.lastMousePos = {
        dot: {
          x: 0,
          y: 0
        },
        circle: {
          x: 0,
          y: 0
        }
      };
      this.lastScale = 1;
      this.lastOpacity = 1;

      this.initEvents();
      requestAnimationFrame(() => this.render());
    }
    initEvents() {
      window.addEventListener('mousemove', ev => this.mousePos = getMousePos(ev));
    }
    render() {
      this.lastMousePos.dot.x = lerp(this.lastMousePos.dot.x, this.mousePos.x - this.bounds.dot.width / 2, 1);
      this.lastMousePos.dot.y = lerp(this.lastMousePos.dot.y, this.mousePos.y - this.bounds.dot.height / 2, 1);
      this.lastMousePos.circle.x = lerp(this.lastMousePos.circle.x, this.mousePos.x - this.bounds.circle.width / 2, 0.15);
      this.lastMousePos.circle.y = lerp(this.lastMousePos.circle.y, this.mousePos.y - this.bounds.circle.height / 2, 0.15);
      this.lastScale = lerp(this.lastScale, this.scale, 0.15);
      this.lastOpacity = lerp(this.lastOpacity, this.opacity, 0.1);
      this.DOM.dot.style.transform = `translateX(${(this.lastMousePos.dot.x)}px) translateY(${this.lastMousePos.dot.y}px)`;
      this.DOM.circle.style.transform = `translateX(${(this.lastMousePos.circle.x)}px) translateY(${this.lastMousePos.circle.y}px) scale(${this.lastScale})`;
      this.DOM.circle.style.opacity = this.lastOpacity
      requestAnimationFrame(() => this.render());
    }
    enter() {
      cursor.scale = 2.7;
      // cursor.querySelector('.cursor__inner--dot').scale = 2.7;
    }
    leave() {
      cursor.scale = 1;
    }
    click() {
      this.lastScale = 1;
      this.lastOpacity = 0;
    }
  }
  // const cursor = new CursorFx(document.querySelector('.cursor'));
  // [...document.querySelectorAll('[data-hover]')].forEach((link) => {
  //     link.addEventListener('mouseenter', () => cursor.enter() );
  //     link.addEventListener('mouseleave', () => cursor.leave() );
  //     link.addEventListener('click', () => cursor.click() );
  // });

  // // Navigation click - go to the Next Slide
  // $slideNavNext.click(function (e) {
  //   e.preventDefault();

  //   var slideOut = $('.homeSlide.active'),
  //       slideIn = $('.homeSlide.active').next('.homeSlide');

  //   goToNextSlide(slideOut, slideIn);

  // });

  // // Go to previous slide - pass 2 parameters - slideOut and slideIn
  // function goToPreviousSlide(slideOut, slideIn){

  //   var tl = new TimelineLite(),
  //     slideOutH1 = slideOut.find('h1'),
  //     slideOutP = slideOut.find('p'),
  //     slideInH1 = slideIn.find('h1'),
  //     slideInP = slideIn.find('p'),
  //     index = slideIn.index(),
  //     size = $('.top .homeSlide').length;

  //   if(slideIn.length !== 0){

  //     // go to the previous slide timeline
  //     tl
  //       // move the new slide (the one about to enter viewport) out of the viewport (to the top)
  //       .set(slideIn, {y: '-100%', autoAlpha: 1, className: '+=active'})
  //       // remove class active from the currently active slide (slideOut)
  //       .set(slideOut, {className: '-=active'})
  //       // animate H1 and p of the active slide down and fade them out
  //       .to([slideOutH1,slideOutP], 0.3, {y: '+=15px', autoAlpha: 0, ease:Power3.easeInOut}, 0)
  //       // animate active slide down (out of the viewport)
  //       .to(slideOut, 0.5, {y: '100%', ease:Power3.easeInOut}, 0)
  //       // animate new slide down (from out of the viewport)
  //       .to(slideIn, 0.5, {y: '+=100%', ease:Power3.easeInOut}, '-=0.5')
  //       // animate H1 and P of the new slide down and fade them in
  //       .fromTo([slideInH1,slideInP], 0.3, {y: '-=20px', autoAlpha: 0}, {autoAlpha: 1, y: 0, ease:Power1.easeInOut}, 0.3);

  //   }

  //   // Fade out arrow down and fade in arrow up

  //   // Fade in arrow up
  //   TweenLite.set($slideNavNext, {autoAlpha: 1});

  //   // Fade out arrow down on first slide
  //   if(index === 1){
  //     TweenLite.to($slideNavPrev, 0.3, {autoAlpha: 0.2, ease:Linear.easeNone});
  //   }

  // }

  // // Navigation click - go to the Prev Slide
  // $slideNavPrev.click(function (e) {
  //   e.preventDefault();

  //   var slideOut = $('.homeSlide.active'),
  //       slideIn = $('.homeSlide.active').prev('.homeSlide');

  //   goToPreviousSlide(slideOut, slideIn);

  // });
})($);