import $ from "jquery";
import "bootstrap";
import {
  TweenMax,
  TimelineLite,
  TimelineMax,
  CSSPlugin,
  ScrollToPlugin,
  Draggable,
  Elastic
} from "gsap/all";
import "bootstrap/dist/css/bootstrap.min.css";
// import "./css/icomoon.css";
import "./scss/ti-styles.scss";
import EasePack from "gsap/EasePack";
import 'pace-js';
import 'select2/dist/css/select2.min.css';
import 'select2/dist/js/select2.full.min.js';
import 'jquery-validation/dist/jquery.validate.min.js';

const plugins = [CSSPlugin, ScrollToPlugin, EasePack];

Pace.start({
  document: false,
  target: '.ti-preloader__progress-count',
  minTime: 9000,
});
Pace.on("done", function () {
  $(".ti-preloader__content").hide();
  $(".ti-preloader-section").fadeOut(1200);
});

(function ($) {

  // Set variables
  var $activeTextSlide = $(".ti-hero-section__slider-text-right.slider-active"),
    $allTextSlides = $(".ti-hero-section__slider-text-right"),
    $activeTextLeftSlide = $(
      ".ti-hero-section__slider-text-left.slider-active"
    ),
    $allTextLeftSlides = $(".ti-hero-section__slider-text-left"),
    $activeImageSlide = $(".ti-hero-section__slider-image.slider-active"),
    $allImageSlides = $(".ti-hero-section__slider-image"),
    $activeNextSlideThumbnail = $(
      ".ti-hero-section__carousel-next-slide-thumbnail-image.slider-active"
    ),
    $allNextSlideThumbnails = $(
      ".ti-hero-section__carousel-next-slide-thumbnail-image"
    ),
    $hero = $(".ti-hero-section__content-wrapper"),
    totalSlides = $allImageSlides.length,
    currentSlide,
    totalThumbnails = $allNextSlideThumbnails.length,
    currentThumbnail,
    $activeDescriptionSlide = $(".ti-hero-section__description.slider-active"),
    $allDescriptionSlides = $(".ti-hero-section__description"),
    $activeCta = $(".ti-hero-section__cta.slider-active"),
    $allCtas = $(".ti-hero-section__cta");

  // Mouse move tilt effect
  $(document).mousemove(function (event) {
    // Detect mouse position
    var xPos = event.clientX / $(window).width() - 0.5;
    var yPos = event.clientY / $(window).height() - 0.5;

    // Tilt elements on hero container
    TweenLite.to($hero, 0.6, {
      rotationY: 3 * xPos,
      rotationX: 3 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 900,
      transformOrigin: "center"
    });
    TweenLite.to(".ti-hero-section__slider-text-right", 0.6, {
      rotationY: 30 * xPos,
      rotationX: 30 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 200,
      transformOrigin: "left"
    });
    TweenLite.to(".ti-hero-section__slider-text-left", 0.6, {
      rotationY: 30 * xPos,
      rotationX: 30 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 200,
      transformOrigin: "right"
    });
    TweenLite.to(".ti-hero-section__slider-images", 0.6, {
      rotationY: 3 * xPos,
      rotationX: 3 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 200,
      transformOrigin: "top"
    });
    TweenLite.to(".ti-hero-section__carousel-next-slide-thumbnail-image", 0.6, {
      rotationY: 20 * xPos,
      rotationX: 20 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 200,
      transformOrigin: "center"
    });
    TweenLite.to(".ti-hero-section__particles-wrapper", 0.6, {
      rotationY: 2 * xPos,
      rotationX: 2 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 900,
      transformOrigin: "center"
    });
    TweenLite.to(".ti-hero-section__carousel-nav-count", 0.6, {
      rotationY: 20 * xPos,
      rotationX: 20 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 200,
      transformOrigin: "center"
    });
    TweenLite.to(".ti-hero-section__details", 0.6, {
      rotationY: 10 * xPos,
      rotationX: 10 * yPos,
      ease: Power1.easeOut,
      transformPerspective: 900,
      transformOrigin: "center"
    });
  });

  //   Hamburger Dropdown Reveal
  var hamburgerButton = document.querySelector(".ti-header__hamburger");
  var dropdownContainer = document.querySelector(
    ".ti-header-dropdown-container"
  );
  var overlay = document.querySelector(".shape-overlays");
  var paths = document.querySelectorAll(".shape-overlays__path");
  var numPoints = 10;
  var numPaths = paths.length;
  var delayPointsMax = 0.3;
  var delayPerPath = 0.25;
  var duration = 0.9;
  var isOpened = false;
  var pointsDelay = [];
  var allPoints = [];
  var ease = Power2.easeInOut;

  var tl = new TimelineMax({
    onUpdate: render
  });
  var itemsTl = new TimelineMax();

  for (var i = 0; i < numPaths; i++) {
    var points = [];
    allPoints.push(points);
    for (var j = 0; j < numPoints; j++) {
      points.push(100);
    }
  }

  hamburgerButton.addEventListener("click", onClick);

  function onClick() {
    if (!tl.isActive()) {
      isOpened = !isOpened;
      toggle();
    }
  }

  function toggle() {
    $("body").toggleClass("ti-dropdown-active");
    console.log("toggle");
    tl.progress(0).clear();
    itemsTl.progress(0).clear();

    for (var i = 0; i < numPoints; i++) {
      pointsDelay[i] = Math.random() * delayPointsMax;
    }

    for (var i = 0; i < numPaths; i++) {
      var points = allPoints[i];
      var pathDelay = delayPerPath * (isOpened ? i : numPaths - i - 1);

      for (var j = 0; j < numPoints; j++) {
        var config = {
          ease: ease
        };
        config[j] = 0;
        var delay = pointsDelay[j];

        tl.to(points, duration, config, delay + pathDelay);
        if ($("body").hasClass("ti-dropdown-active")) {
          dropdownContainer.style.display = "block";
          tl.eventCallback("onComplete", function () {
            itemsTl.staggerFromTo(
              ".ti-header-dropdown__item-list > li",
              1, {
                y: "90px",
                autoAlpha: 0
              }, {
                y: "0",
                autoAlpha: 1
              },
              "0.3",
              "-=0.2"
            );
            $("body").addClass("ti-dropdown-active");
          });
        } else {
          tl.eventCallback("onComplete", null);
          setTimeout(function () {
            dropdownContainer.style.display = "none";
          }, 1500);
        }
      }
    }
  }

  function render() {
    console.log("render");
    for (var i = 0; i < numPaths; i++) {
      var path = paths[i];
      var points = allPoints[i];

      var d = "";
      d += isOpened ? `M 0 0 V ${points[0]} C` : `M 0 ${points[0]} C`;

      for (var j = 0; j < numPoints - 1; j++) {
        var p = ((j + 1) / (numPoints - 1)) * 100;
        var cp = p - ((1 / (numPoints - 1)) * 100) / 2;
        d += ` ${cp} ${points[j]} ${cp} ${points[j + 1]} ${p} ${points[j + 1]}`;
      }

      d += isOpened ? ` V 100 H 0` : ` V 0 H 0`;
      path.setAttribute("d", d);
    }
  }

  // Some help functions.
  const shuffleArray = arr => arr.sort(() => Math.random() - 0.5);
  const lineEq = (y2, y1, x2, x1, currentVal) => {
    let m = (y2 - y1) / (x2 - x1);
    let b = y1 - m * x1;
    return m * currentVal + b;
  };
  const lerp = (a, b, n) => (1 - n) * a + n * b;
  const body = document.body;
  const bodyColor = getComputedStyle(body).getPropertyValue('--color-bg').trim() || 'white';
  const getMousePos = (e) => {
    let posx = 0;
    let posy = 0;
    if (!e) e = window.event;
    if (e.pageX || e.pageY) {
      posx = e.pageX;
      posy = e.pageY;
    } else if (e.clientX || e.clientY) {
      posx = e.clientX + body.scrollLeft + document.documentElement.scrollLeft;
      posy = e.clientY + body.scrollTop + document.documentElement.scrollTop;
    }
    return {
      x: posx,
      y: posy
    }
  }
  class CursorFx {
    constructor(el) {
      this.DOM = {
        el: el
      };
      this.DOM.dot = this.DOM.el.querySelector('.cursor__inner--dot');
      this.DOM.circle = this.DOM.el.querySelector('.cursor__inner--circle');
      this.bounds = {
        dot: this.DOM.dot.getBoundingClientRect(),
        circle: this.DOM.circle.getBoundingClientRect()
      };
      this.scale = 1;
      // this.opacity = 1;
      this.mousePos = {
        x: 0,
        y: 0
      };
      this.lastMousePos = {
        dot: {
          x: 0,
          y: 0
        },
        circle: {
          x: 0,
          y: 0
        }
      };
      this.lastScale = 1;
      this.lastOpacity = 1;

      this.initEvents();
      requestAnimationFrame(() => this.render());
    }
    initEvents() {
      window.addEventListener('mousemove', ev => this.mousePos = getMousePos(ev));
    }
    render() {
      this.lastMousePos.dot.x = lerp(this.lastMousePos.dot.x, this.mousePos.x - this.bounds.dot.width / 2, 1);
      this.lastMousePos.dot.y = lerp(this.lastMousePos.dot.y, this.mousePos.y - this.bounds.dot.height / 2, 1);
      this.lastMousePos.circle.x = lerp(this.lastMousePos.circle.x, this.mousePos.x - this.bounds.circle.width / 2, 0.15);
      this.lastMousePos.circle.y = lerp(this.lastMousePos.circle.y, this.mousePos.y - this.bounds.circle.height / 2, 0.15);
      this.lastScale = lerp(this.lastScale, this.scale, 0.15);
      this.lastOpacity = lerp(this.lastOpacity, this.opacity, 0.1);
      this.DOM.dot.style.transform = `translateX(${(this.lastMousePos.dot.x)}px) translateY(${this.lastMousePos.dot.y}px)`;
      this.DOM.circle.style.transform = `translateX(${(this.lastMousePos.circle.x)}px) translateY(${this.lastMousePos.circle.y}px) scale(${this.lastScale})`;
      this.DOM.circle.style.opacity = this.lastOpacity
      requestAnimationFrame(() => this.render());
    }
    enter() {
      cursor.scale = 2.7;
      // cursor.querySelector('.cursor__inner--dot').scale = 2.7;
    }
    leave() {
      cursor.scale = 1;
    }
    click() {
      this.lastScale = 1;
      this.lastOpacity = 0;
    }
  }
  // const cursor = new CursorFx(document.querySelector('.cursor'));
  // [...document.querySelectorAll('[data-hover]')].forEach((link) => {
  //     link.addEventListener('mouseenter', () => cursor.enter() );
  //     link.addEventListener('mouseleave', () => cursor.leave() );
  //     link.addEventListener('click', () => cursor.click() );
  // });

  $(".ti-back-button").click(function () {
    if (document.referrer == "") {
      document.location.href = "/";
    } else {
      window.history.back()
    }
  });
  $(".ti-form-select").select2({
    minimumResultsForSearch: -1
  });
  //Form Validation
  var email = document.getElementById('email'),
    name = document.getElementById('name'),
    subject = document.getElementById('subject'),
    submit = document.getElementById('contactFormSubmitButton'),
    messagee = document.getElementById('message');
  $('#subject').on('change', function () {
    $(this).valid();
  });
  $.validator.addMethod("valueNotEquals", function (value, element, args) {
    if (value == "Choose a country") {
      return false;
    } else {
      return true;
    }
  }, "Please select a country");

  var form = $("#contactForm");
  form.validate({
    ignore: [],
    rules: {
      name: {
        required: true
      },
      subject: {
        required: true,
      },
      email: {
        required: true,
        email: true
      }
    },
    errorPlacement: function errorPlacement(error, element) {
      if (element.attr("name") == "subject") {
        var customSelect = $(element).closest(".ti-form-input-container").find(".select2");
        error.insertAfter(customSelect);
      } else {
        error.insertAfter(element);
      }
    },
    highlight: function highlight(element) {
      $(element).closest(".ti-form-input-container").addClass("ti-error");
    },
    unhighlight: function unhighlight(element) {
      $(element).closest(".ti-form-input-container").removeClass("ti-error");
    }
  });

  //Form Submission
  var successfulSignup = function successfulSignup(response) {
    response = JSON.parse(response);
    if (response.status === 'success' && response.devCode === 101) {
      $("input[type=text], input[type=email], textarea").val("");
      $(".ti-form-notification").html("Success! I will reach out to you shortly.").slideDown();
      setTimeout(function () {
        $(".ti-form-notification").slideUp();
      }, 4000);
      // $("#contactForm").css("display", "none");
      // $(".ay-contact-form__success-message-wrapper").fadeIn();
    } else if (response.status === 'success' && response.devCode === 102) {
      // $("#contactForm").css("display", "none");
      // console.log("It seems you’re already on the list. We’ll be in touch shortly.");
    } else {
      // $("#contactForm").hide();
      // console.log("Something went wrong, please refresh the page and try again.");
      // alert("Something went wrong, please refresh the page and try again.");
      $(".ti-form-notification").html("Oops! Something went wrong, please try again later.").slideDown();
      setTimeout(function () {
        $(".ti-form-notification").slideUp();
      }, 4000);
    }
  };
  form.submit(function (event) {
    if (form.valid()) {
      var emailAddress = email.value,
        contactName = name.value,
        subject_val = subject.value,
        message_val = messagee.value;

      var data = JSON.stringify({
        'email': emailAddress,
        'name': contactName,
        'subject': subject_val,
        'messagee': message_val
      });
      // $("#loader").css("display", "block");

      // console.log(data);
      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === this.DONE) {
          console.log(this.responseText);
          // $("#loader").css("display", "none");
          // $("#message").css({ "color": "#d2737b" }, { "display": "block" });
          // $("#message").html(" Invalid Email address");
          successfulSignup(this.responseText);
        }
      });

      xhr.open("POST", "request.php");
      xhr.setRequestHeader("content-type", "application/json");
      xhr.send(data);
    }
    event.preventDefault(); // stop form from redirecting to java servlet page
  });

  // Fix for validation issue on select2 dropdowns
  $('body').on('change', '.ay-country-select', function () {
    $(this).valid();
  });

})($);