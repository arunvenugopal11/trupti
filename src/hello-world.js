import HelloWorldButton from './components/hello-world-button/hello-world-button.js';
import Heading from './components/heading/heading.js'
import React from 'react'

const heading = new Heading();
heading.render('hello world');
const hellowWorldButton = new HelloWorldButton();
hellowWorldButton.render();

if(process.env.NODE_ENV === 'production'){
    console.log("Production Mode");
} else if(process.env.NODE_ENV === 'development'){
    console.log("Development mode");
}

hellowWorldButton.methodThatNotExists();