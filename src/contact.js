import $ from "jquery";
import "bootstrap";
import {
    TweenMax,
    TimelineLite,
    TimelineMax,
    CSSPlugin,
    ScrollToPlugin,
    Draggable,
    Elastic
} from "gsap/all";
import "bootstrap/dist/css/bootstrap.min.css";
// import "./css/icomoon.css";
import "./scss/ti-styles.scss";
import EasePack from "gsap/EasePack";
import 'pace-js';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick.min.js';
import 'jquery-validation/dist/jquery.validate.min.js';


const plugins = [CSSPlugin, ScrollToPlugin, EasePack];

Pace.start({
    document: false,
    target: '.ti-preloader__progress-count',
    minTime: 9000,
});
Pace.on("done", function () {
    $(".ti-preloader__content").hide();
    $(".ti-preloader-section").fadeOut(1200);
});



(function ($) {
    $(document).ready(function () {
        // Some help functions.
        const shuffleArray = arr => arr.sort(() => Math.random() - 0.5);
        const lineEq = (y2, y1, x2, x1, currentVal) => {
            let m = (y2 - y1) / (x2 - x1);
            let b = y1 - m * x1;
            return m * currentVal + b;
        };
        const lerp = (a, b, n) => (1 - n) * a + n * b;
        const body = document.body;
        const bodyColor = getComputedStyle(body).getPropertyValue('--color-bg').trim() || 'white';
        const getMousePos = (e) => {
            let posx = 0;
            let posy = 0;
            if (!e) e = window.event;
            if (e.pageX || e.pageY) {
                posx = e.pageX;
                posy = e.pageY;
            } else if (e.clientX || e.clientY) {
                posx = e.clientX + body.scrollLeft + document.documentElement.scrollLeft;
                posy = e.clientY + body.scrollTop + document.documentElement.scrollTop;
            }
            return {
                x: posx,
                y: posy
            }
        }

        class CursorFx {
            constructor(el) {
                this.DOM = {
                    el: el
                };
                this.DOM.dot = this.DOM.el.querySelector('.cursor__inner--dot');
                this.DOM.circle = this.DOM.el.querySelector('.cursor__inner--circle');
                this.bounds = {
                    dot: this.DOM.dot.getBoundingClientRect(),
                    circle: this.DOM.circle.getBoundingClientRect()
                };
                this.scale = 1;
                // this.opacity = 1;
                this.mousePos = {
                    x: 0,
                    y: 0
                };
                this.lastMousePos = {
                    dot: {
                        x: 0,
                        y: 0
                    },
                    circle: {
                        x: 0,
                        y: 0
                    }
                };
                this.lastScale = 1;
                this.lastOpacity = 1;

                this.initEvents();
                requestAnimationFrame(() => this.render());
            }
            initEvents() {
                window.addEventListener('mousemove', ev => this.mousePos = getMousePos(ev));
            }
            render() {
                this.lastMousePos.dot.x = lerp(this.lastMousePos.dot.x, this.mousePos.x - this.bounds.dot.width / 2, 1);
                this.lastMousePos.dot.y = lerp(this.lastMousePos.dot.y, this.mousePos.y - this.bounds.dot.height / 2, 1);
                this.lastMousePos.circle.x = lerp(this.lastMousePos.circle.x, this.mousePos.x - this.bounds.circle.width / 2, 0.15);
                this.lastMousePos.circle.y = lerp(this.lastMousePos.circle.y, this.mousePos.y - this.bounds.circle.height / 2, 0.15);
                this.lastScale = lerp(this.lastScale, this.scale, 0.15);
                this.lastOpacity = lerp(this.lastOpacity, this.opacity, 0.1);
                this.DOM.dot.style.transform = `translateX(${(this.lastMousePos.dot.x)}px) translateY(${this.lastMousePos.dot.y}px)`;
                this.DOM.circle.style.transform = `translateX(${(this.lastMousePos.circle.x)}px) translateY(${this.lastMousePos.circle.y}px) scale(${this.lastScale})`;
                this.DOM.circle.style.opacity = this.lastOpacity
                requestAnimationFrame(() => this.render());
            }
            enter() {
                cursor.scale = 2.7;
                // cursor.querySelector('.cursor__inner--dot').scale = 2.7;
            }
            leave() {
                cursor.scale = 1;
            }
            click() {
                this.lastScale = 1;
                this.lastOpacity = 0;
            }
        }
        // const cursor = new CursorFx(document.querySelector('.cursor'));
        // [...document.querySelectorAll('[data-hover]')].forEach((link) => {
        //     link.addEventListener('mouseenter', () => cursor.enter());
        //     link.addEventListener('mouseleave', () => cursor.leave());
        //     link.addEventListener('click', () => cursor.click());
        // });
    });

    $(".ti-back-button").click(function () {
        if (document.referrer == "") {
            document.location.href = "/";
        } else {
            window.history.back()
        }
    });

    //Form
    var email = document.getElementById('email'),
        profile = document.getElementById('profile'),
        fullName = document.getElementById('fullname'),
        description = document.getElementById('description');
    var rates = document.getElementsByName('Service');

    var submit = document.getElementById('contactSubmitButton');

    var form = $("#contactForm");
    form.validate({
        ignore: [],
        rules: {
            fullName: {
                required: true
            },
            profile: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            Service: {
                required: true
            },
            description: {
                required: true,
            }
        },
        messages: {
            Service: {
                required: "Please select a service"
            }
        },
        highlight: function highlight(element) {
            $(element).closest(".form-group").addClass("ti-error");
        },
        unhighlight: function unhighlight(element) {
            $(element).closest(".form-group").removeClass("ti-error");
        },
        errorPlacement: function (error, element) {
            if (element.is(":radio")) {
                console.log(element);
                error.appendTo(element.parents('.ti-contact__radio-btn-wrapper'));
            } else { // This is the default behavior 
                error.insertAfter(element);
            }
        }
    });
    //Form Submission
    var successfulSignup = function successfulSignup(response) {
        response = JSON.parse(response);
        if (response.status === 'success' && response.devCode === 101) {
            $("input[type=text], input[type=email], textarea").val("");
            document.getElementById("subscription").checked = false;
            document.getElementById('serviceIdDesign').checked = false;
            document.getElementById('serviceIdCoching').checked = false;

            $(".ti-form-notification").html("Success! I will reach out to you shortly.").slideDown();
            setTimeout(function () {
                $(".ti-form-notification").slideUp();
            }, 4000);
            // $("#contactForm").css("display", "none");
            // $(".ay-contact-form__success-message-wrapper").fadeIn();
        } else if (response.status === 'success' && response.devCode === 102) {
            // $("#contactForm").css("display", "none");
            // console.log("It seems you’re already on the list. We’ll be in touch shortly.");
        } else {
            // $("#contactForm").hide();
            // console.log("Something went wrong, please refresh the page and try again.");
            // alert("Something went wrong, please refresh the page and try again.");
            $(".ti-form-notification").html("Oops! Something went wrong, please try again later.").slideDown();
            setTimeout(function () {
                $(".ti-form-notification").slideUp();
            }, 4000);
        }
    };

    form.submit(function (event) {
        event.preventDefault(); // stop form from redirecting to java servlet page
        if (form.valid()) {
            var rate_value;
            for (var i = 0; i < rates.length; i++) {
                if (rates[i].checked) {
                    rate_value = rates[i].value;
                    console.log(rate_value);
                }
            }
            var full_name = fullName.value,
                profile_val = profile.value,
                email_val = email.value,
                service_val = rate_value,
                description_val = description.value;
            var subscription_val;
            if ($('#subscription').is(":checked")) {
                subscription_val = 'Yes';
            } else {
                subscription_val = 'No';
            };

            var data = JSON.stringify({
                'fullname': full_name,
                'profile': profile_val,
                'email': email_val,
                'service': service_val,
                'description': description_val,
                'subscription': subscription_val,
            });
            // $("#loader").css("display", "block");

            var xhr = new XMLHttpRequest();

            xhr.addEventListener("readystatechange", function () {
                if (this.readyState === this.DONE) {
                    console.log(this.responseText);
                    // $("#loader").css("display", "none");
                    // $("#message").css({ "color": "#d2737b" }, { "display": "block" });
                    // $("#message").html(" Invalid Email address");
                    successfulSignup(this.responseText);
                }
            });

            xhr.open("POST", "./request_contact.php");
            xhr.setRequestHeader("content-type", "application/json");
            xhr.send(data);
        }
    });

})($);