<?php
//ini_set('display_errors', 0);

header('Content-Type: application/json');
require_once('./vendor/autoload.php');
use Postmark\PostmarkClient;
use Postmark\Models\PostmarkException;

//$data = json_decode(file_get_contents('php://input'), true);
$pdata = json_decode(file_get_contents('php://input'));
$email	= trim(htmlspecialchars($pdata->email));
$name = trim(htmlspecialchars($pdata->fullname));
$profile = trim(htmlspecialchars($pdata->profile));
$service	= trim(htmlspecialchars($pdata->service));
$description = trim(htmlspecialchars($pdata->description));
$subscription = trim(htmlspecialchars($pdata->subscription));

// Send an email to the team
// hello@truptishirodkar.com
try{
    if($email){
        $client = new PostmarkClient("c49a66b9-37b0-4844-9b96-180a6bc8b18f");
        $sendResult = $client->sendEmail("hello@truptishirodkar.com",
        "postmark@truptishirodkar.com",
        "Let's talk: {$name}",
        "<strong>Full name:</strong> {$name} <br> <strong>Email:</strong> {$email} <br> <strong>Profile/Company :</strong> {$profile} <br> <strong>Service :</strong> {$service} <br> <strong>Subscription :</strong> {$subscription} <br> <strong>Description :</strong> {$description} ");
        $data['status']="success";
        $data['devCode']=101;
        echo json_encode($data);
    }

}catch(PostmarkException $ex){
        // If client is able to communicate with the API in a timely fashion,
        // but the message data is invalid, or there's a server error,
        // a PostmarkException can be thrown.
        // echo $ex->httpStatusCode;
        // echo $ex->message;
        // echo $ex->postmarkApiErrorCode;
        $data["status"]=$ex->httpStatusCode;
        $data["msg"]=$ex->message;
        $data["postmarkApiErrorCode"]=$ex->postmarkApiErrorCode;
        echo json_encode($data);

}

?>